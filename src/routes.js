import WelcomePage from './components/WelcomePage.vue';
import ViewAllPage from './components/ViewAllPage.vue';
import AddPlanetPage from './components/AddPlanetPage.vue';
import DeletePlanetPage from './components/DeletePlanetPage.vue';
import UpdatePlanetPage from './components/UpdatePlanetPage.vue';

const routes = [
    {path: '/', component: WelcomePage },
    {path: '/viewAll/:apiKey', name: 'ViewAll', component: ViewAllPage},
    {path: '/addPlanet/:apiKey', name: 'AddPlanet', component: AddPlanetPage},
    {path: '/deletePlanet/:apiKey', name: 'DeletePlanet', component: DeletePlanetPage},
    {path: '/updatePlanet/:apiKey', name: 'UpdatePlanet', component: UpdatePlanetPage}

];

export default routes;